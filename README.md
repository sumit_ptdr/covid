# Covid Tracker

[![Header](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRehekDFyiXs4SfXKq5IzkdX1EMB_GtLbc6fA&usqp=CA, "Header")](https://thetechnohack.cf/)

# Hello, there!
Here is a cool python tool for tracking global Covid-19 status.

## Screenshot
![screenshot](https://bitbucket.org/sumit_ptdr/covid/src/main/screenshot.jpg)

## Requirements
- Python v3+

## Installation
```
pkg install python
pip install -r requirements.txt
```
## Usage
```
python covid19.py
```

## Features
- Track status with country name or id.
- Easy to use.
- Accurate and fast data delivery.

## Technologies & Tools
![](https://img.shields.io/badge/OS-Linux-informational?style=flat&logo=linux&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-Python-informational?style=flat&logo=python&logoColor=white&color=2bbc8a)
